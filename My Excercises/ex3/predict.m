function p = predict(Theta1, Theta2, X)
%PREDICT Predict the label of an input given a trained neural network
%   p = PREDICT(Theta1, Theta2, X) outputs the predicted label of X given the
%   trained weights of a neural network (Theta1, Theta2)

% Useful values
m = size(X, 1);
num_labels = size(Theta2, 1);

% You need to return the following variables correctly 
p = zeros(size(X, 1), 1);

% add 1 column to X
X = [ones(size(X,1),1) X]

% ====================== YOUR CODE HERE ======================
% Instructions: Complete the following code to make predictions using
%               your learned neural network. You should set p to a 
%               vector containing labels between 1 to num_labels.
%
% Hint: The max function might come in useful. In particular, the max
%       function can also return the index of the max element, for more
%       information see 'help max'. If your examples are in rows, then, you
%       can use max(A, [], 2) to obtain the max for each row.
%

% calculate the z(2) = X * Theta1' (mx25 matrix)
z2 = X * Theta1';
% calculate the activation a(2) = sigmoid(z(2))
a2 = sigmoid(z2);
% add 1-first column to a2 (a2 becomes mx26 matrix)
a2 = [ones(size(a2,1),1) a2];

% calculate the z(3) = a(2) * Theta2' (mx10 matrix)
z3 = a2 * Theta2';
% calculate the activation a(3) = sigmoid(z(3))
a3 = sigmoid(z3);

% prediction vector (m-dimensional for m examples)
[value, p] = max(a3, [], 2);

% =========================================================================

end
