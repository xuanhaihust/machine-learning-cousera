function theta = Logistic_Regression(initial_theta, X, y, iterNum, alpha, lambda)
    
    % alpha = 1    
    theta = initial_theta;
    m = length(y);
    
    for i = 1:iterNum
        [cost, grad] = costFunctionReg(theta,X,y,lambda);
        theta = theta*(1-lambda*alpha/m) - grad;
    end
end